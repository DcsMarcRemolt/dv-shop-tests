export function clearInput(selector: string) {
  /*
      setValue with empty space does not trigger input event => no reaction from angular
      same goes for
      document.querySelector('#test').value = ''
      so this is a known browser problem.

      As a solution we clear the input with '' and then manually fire the input event.
    */
  browser.selectorExecute(selector, (elements: any[]) => {
    const e = new Event('input');
    const currentInput: HTMLInputElement = elements[0];
    currentInput.value = '';
    currentInput.dispatchEvent(e);
    currentInput.blur();
  });
}
