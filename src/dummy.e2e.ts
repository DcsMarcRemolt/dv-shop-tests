import { expect } from 'chai';

describe('the testsuite', () => {
  it('runs', () => {
    expect(42).to.equal(42);
    expect(44).to.not.equal(42);
  });
});
