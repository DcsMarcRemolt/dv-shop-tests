import { expect } from 'chai';

export class ProductDetailPage {
  get productDetails() {
    return browser.element('article#productdetails');
  }

  get productTitle() {
    return this.productDetails.getText('h1.titel');
  }

  get productArticleNr() {
    return this.productDetails.getText('h4');
  }

  get leistungenSection() {
    return browser.element('article.collapse-area:nth-child(4)');
  }

  get leistungenSectionLabel() {
    return this.leistungenSection.element('label');
  }

  get leistungenSectionDetails() {
    return this.leistungenSection.element('section.details');
  }
}

describe('product detail page', () => {
  let page: ProductDetailPage;

  beforeEach(() => {
    page = new ProductDetailPage();
    browser.url('/web/de/datev-shop/office-management/anwalt-classic-pro/');
    browser.waitForExist('h1.titel');
  });

  describe('head section', () => {
    it('shows the article number', () => {
      expect(page.productArticleNr).to.include('48611');
    });

    it('shows the product title', () => {
      expect(page.productTitle).to.equal('Anwalt classic');
    });
  });

  describe('Leistungen', () => {
    it('is collapsed on load', () => {
      expect(page.leistungenSectionDetails.isVisible()).to.be.false;
    });

    it('expands when clicking on the label', () => {
      page.leistungenSectionLabel.click();
      expect(page.leistungenSectionDetails.isVisible()).to.be.true;
      browser.saveScreenshot('screenshots/detail-page-leistungen.png');
    });

    it('example for test debugging a step', () => {
      page.leistungenSectionLabel.click();
      const visible = page.leistungenSectionDetails.isVisible();
      debugger;
    }).timeout(99999999999999);
  });
});
