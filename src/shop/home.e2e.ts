import { expect } from 'chai';

describe('shop homepage', () => {
  beforeEach(() => {
    browser.url('/web/de/datev-shop/');
  });

  it('displays the right title', () => {
    expect(browser.getTitle()).to.equal('Shop-Suche in datev.de');
  });

  describe('product list', () => {
    beforeEach(() => {
      // wait until products are visible on the page
      browser.waitForExist('article.listentry');
    });

    describe('default view', () => {
      it('displays the number of products', () => {
        expect(browser.getText('#bw-resultsCount')).to.equal('2524');
        // better for dynamic content, test the correct format
        expect(browser.getText('#bw-resultsCount')).to.match(/[1-9][0-9]+/);
      });

      it('shows the first 10 products', () => {
        expect(browser.elements('article.listentry').value.length).to.equal(10);
      });
    });

    describe('loading more products', () => {
      it('shows 20 products after clicking on "Weitere Treffer laden"', () => {
        browser.waitForExist('#bw-pagerlink');
        browser.click('#bw-pagerlink');
        // pause until the wait animation at the bottom is gone
        browser.waitForExist('#bw-pagerWaitText', 5000, true);

        expect(browser.elements('article.listentry').value.length).to.equal(20);
      });
    });

    describe('filtering for Berufsgruppe', () => {
      let numberOfLawyers: string;

      beforeEach(() => {
        numberOfLawyers = browser.getText(
          'label[title="Rechtsanwälte"] span.bw-count'
        );

        browser.click('#bw-Rechtsanwälte-radio');
        browser.waitForExist(
          '#bw-Berufsgruppe-selection[title="Rechtsanwälte"]'
        );
      });

      afterEach(() => {
        // even when visiting the base URL (without the query params), the site redirects to the URL with params
        // unless we manually delete the cookie and session storage key (this smells like bad code app side)
        browser.execute(() => {
          // we are now IN the browser

          // remove the cookie selecting the Berufsgruppe
          document.cookie =
            'bg=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/; domain=.datev.de';
          // remove the session storage marking the Berufsgruppe section as collapsed
          sessionStorage.removeItem('bw-Berufsgruppe');
        });
      });

      it('shows the lower number of products', () => {
        browser.waitForExist('#bw-resultsCount');
        expect(browser.getText('#bw-resultsCount')).to.equal('445');
        // better, take the number from the Berufsgruppe selector
        expect(browser.getText('#bw-resultsCount')).to.equal(numberOfLawyers);
      });

      it('reloads the page with a new URL', () => {
        expect(browser.getUrl()).to.have.string(
          '/web/de/datev-shop/?query=&Berufsgruppe=Rechtsanw%C3%A4lte'
        );
      });
    });
  });
});
